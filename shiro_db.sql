-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3307
-- Generation Time: 20 Nov 2017 pada 14.12
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shiro_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_barang`
--

CREATE TABLE `tbl_barang` (
  `id_barang` int(11) NOT NULL,
  `nama_barang` varchar(250) NOT NULL,
  `harga_barang` varchar(50) NOT NULL,
  `deskripsi_barang` varchar(250) NOT NULL,
  `gambar` varchar(250) NOT NULL,
  `alamat_penjual` varchar(250) DEFAULT NULL,
  `email_penjual` varchar(50) DEFAULT NULL,
  `id_penjual` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_barang`
--

INSERT INTO `tbl_barang` (`id_barang`, `nama_barang`, `harga_barang`, `deskripsi_barang`, `gambar`, `alamat_penjual`, `email_penjual`, `id_penjual`) VALUES
(2, 'buku', '2000', 'buku adalah ....', 'gambar-14112017212345.jpg', 'yogyakarta', 'reskym@gmail.com', NULL),
(3, 'baju', '150000', 'baju adalah...', 'gambar-14112017224941.jpg', 'yogyakarta', 'anggrek@gmail.com', NULL),
(4, 'gambar', '20000', 'gambar adalah...', 'gambar-14112017225325.jpg', 'sulawesi selatan', 'admin@gmail.com', NULL),
(5, 'lukisan', '70000', 'lukisan ini merupakan', 'gambar-14112017230416.jpg', 'Jl. Babarsari no. 50', 'admin@gmail.com', NULL),
(6, 'baju baru', '150000', 'baju adalah.....', 'gambar-15112017134437.jpg', 'yogyakarta', 'admin@gmail.com', NULL),
(7, 'gambar persahabatan', '10000', 'merupakan sebuah gambar yang melambangkan persahabatan', 'gambar-15112017222337.jpg', 'yogyakarta', 'admin@gmail.com', NULL),
(8, 'design baju', '10000', 'design baju merupakan salah satu produk', 'gambar-16112017063745.jpg', 'yogyakarta', 'admin@gmail.com', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_customer`
--

CREATE TABLE `tbl_customer` (
  `id` int(11) NOT NULL,
  `nama` varchar(250) NOT NULL,
  `tanggal_lahir` varchar(250) NOT NULL,
  `alamat` varchar(250) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_customer`
--

INSERT INTO `tbl_customer` (`id`, `nama`, `tanggal_lahir`, `alamat`, `email`, `password`) VALUES
(1, 'resky', '01 juni 1996', 'yogyakarta', 'reskym@gmail.com', '12345'),
(22, 'try', '07  juli 1996', 'yogyakarta', 'anggrek@gmail.com', 'anggrek'),
(1234567890, 'rista', '07 Juli 1996', 'sulawesi selatan', 'rista@gmail.com', 'rista'),
(2147483647, 'rizky', '01 juni 1996', 'yogyakarta', 'rizky@gmail.com', 'rizky');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_order`
--

CREATE TABLE `tbl_order` (
  `id_order` int(11) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `harga_barang` varchar(50) NOT NULL,
  `deskripsi_barang` varchar(250) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `nama` varchar(250) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `nama`, `username`, `password`) VALUES
(1, 'resky', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` varchar(50) NOT NULL,
  `nama_barang` varchar(100) DEFAULT NULL,
  `harga_barang` varchar(50) DEFAULT NULL,
  `tanggal_transaksi` varchar(50) DEFAULT NULL,
  `nama_pemesan` varchar(50) DEFAULT NULL,
  `alamat_pemesan` varchar(100) DEFAULT NULL,
  `bukti_pembayaran` varchar(50) DEFAULT NULL,
  `email_penjual` varchar(50) DEFAULT NULL,
  `status_transaksi` varchar(50) DEFAULT NULL,
  `gambar` varchar(50) DEFAULT NULL,
  `id_penjual` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `nama_barang`, `harga_barang`, `tanggal_transaksi`, `nama_pemesan`, `alamat_pemesan`, `bukti_pembayaran`, `email_penjual`, `status_transaksi`, `gambar`, `id_penjual`) VALUES
('0', 'buku', '2000', NULL, 'rista', 'sulawesi selatan', 'gambar-16112017015325.jpg', 'reskym@gmail.com', NULL, 'gambar-14112017212345.jpg', NULL),
('trk-16112017020839', 'gambar persahabatan', '10000', NULL, 'rista', 'sulawesi selatan', 'gambar-16112017020839.jpg', 'admin@gmail.com', 'Complete', 'gambar-15112017222337.jpg', 1),
('trk-16112017021010', 'baju baru', '150000', NULL, 'rizky', 'yogyakarta', 'gambar-16112017021010.jpg', 'admin@gmail.com', NULL, 'gambar-15112017134437.jpg', NULL),
('trk-16112017021131', 'baju baru', '150000', '2017-11-16', 'anggrek', 'yogyakarta', 'gambar-16112017021131.jpg', 'admin@gmail.com', NULL, 'gambar-15112017134437.jpg', NULL),
('trk-16112017065042', 'design baju', '10000', '2017-11-16', 'anggrek', 'kalimantan', 'gambar-16112017065042.jpg', 'admin@gmail.com', 'On Progress', 'gambar-16112017063745.jpg', 1),
('trk-16112017084400', 'gambar persahabatan', '10000', '2017-11-16', 'calvin', 'JL. Babarsari', 'gambar-16112017084400.jpg', 'admin@gmail.com', 'On Progress', 'gambar-15112017222337.jpg', 1),
('trk-16112017112946', 'design baju', '10000', '2017-11-16', 'resky', 'yogyakarta', 'gambar-16112017112946.jpg', 'admin@gmail.com', 'On Progress', 'gambar-16112017063745.jpg', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `tanggal_lahir` varchar(250) NOT NULL,
  `alamat` varchar(250) NOT NULL,
  `no_rek` varchar(50) DEFAULT NULL,
  `nama_bank` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `nama`, `email`, `password`, `tanggal_lahir`, `alamat`, `no_rek`, `nama_bank`) VALUES
(1, 'resky', 'admin@gmail.com', 'admin', '01 agustus 1996', 'yogyakarta', '5521094839340', 'BNI'),
(2, 'anggrek', 'anggrek@gmail.com', 'anggrek', '01 juni 1996', 'kalimantan', '5521094839120', 'BNI');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`id_order`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2147483647;
--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
