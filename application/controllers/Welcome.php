<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('login_model');
		$databarang['data']	= $this->login_model->tampil()->result_object();
		$this->load->view('Utama',$databarang);
	}

	public function about()
	{
		$this->load->view('About');
	}

	public function halaman_utama_login()
	{
		$this->load->view('utama_login');
	}

	public function register()
	{
		$this->load->view('register');
	}

	public function halaman_upload($id)
	{
		$this->load->model('login_model');
		$data['data'] = $this->login_model->username($id);
		$this->load->view('input_barang', $data);
	}

	public function username($email)
	{
		$this->load->model('login_model');
		$data['data'] = $this->login_model->ambil_username($email);
		$databarang['data']	= $this->login_model->tampil()->result_object();
		$tampil = $data + $databarang;
		$this->load->view('utama_login',$tampil);    	
	}

	public function addregister()
	{
		$nama			= $this->input->post('nama');
		$email 			= $this->input->post('email');
		$password 		= $this->input->post('password');
		$tanggal_lahir	= $this->input->post('tanggal_lahir');
		$alamat			= $this->input->post('alamat');

		$datacustomer = array(
			'nama'					=> $nama,
			'email'					=> $email,
			'password'				=> $password,
			'tanggal_lahir'			=> $tanggal_lahir,
			'alamat'				=> $alamat
			);
		$this->load->model('login_model');
		$this->login_model->add($datacustomer);
		redirect('#');
	}

	public function tambahbarang()
	{
        $config['upload_path'] = realpath('files');
        $config['allowed_types'] = 'jpg|jpeg';//'txt|pdf|doc|docx|xls|xlsx'; //type file
        $config['max_size']    = '5120'; //maksimal 5 mb
        $config['file_name']      = 'gambar-'.trim(str_replace(" ","",date('dmYHis')));
        //load upload class library
        $this->upload->initialize($config);
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('gambar_barang'))
        {
            // case - failure
            
            $data['success_msg'] = '<div class="alert alert-danger text-center ">' . $this->upload->display_errors(). '</strong> !</div>';
            $this->load->view('input_barang',$data); 

        }
        else
        {
            // case - success
	        $nama_barang						= $this->input->post('nama_barang');
			$harga_barang 						= $this->input->post('harga_barang');
			$deskripsi_barang 					= $this->input->post('deskripsi_barang');
			$alamat								= $this->input->post('alamat');
			$email								= $this->input->post('email');
			$id_penjual							= $this->input->post('id');
            $upload_data = $this->upload->data();
            $file_id = array(
            			'nama_barang'			=> $nama_barang,
						'harga_barang'			=> $harga_barang,
						'deskripsi_barang'		=> $deskripsi_barang,
		         		'gambar' 				=> $upload_data['file_name'],
		         		'alamat'				=> $alamat,
						'email'					=> $email,
						'id_penjual'			=> $id_penjual
		         	);
            $this->load->model('login_model');
			$this->login_model->tambah_barang($file_id);
            $data['success_msg'] = '<div class="alert alert-success text-center ">Gambar <strong>' . $upload_data['file_name'] . '</strong> telah berhasil di unggah!</div>';
            redirect('welcome','refresh');
	            //redirect('utama','refresh');
        }
	}

	public function tampilbeli($id_barang)
	{
		$this->load->model('login_model');
		$databarang['data']	= $this->login_model->tampil_beli($id_barang)->result_object();
		$this->load->view('pembelian_view',$databarang);
	}

	public function transaksi($id_barang)
	{
		$this->load->model('login_model');
		$databarang['data']	= $this->login_model->tampil_beli($id_barang)->result_object();
		$this->load->view('transaksi_view',$databarang);
	}

	public function tambahtransaksi($id_barang)
	{
		$config['upload_path'] = realpath('files');
        $config['allowed_types'] = 'jpg|jpeg';//'txt|pdf|doc|docx|xls|xlsx'; //type file
        $config['max_size']    = '5120'; //maksimal 5 mb
        $config['file_name']      = 'gambar-'.trim(str_replace(" ","",date('dmYHis')));
        //$config['id_transaksi']			='trk-'.trim(str_replace(" ","",date('dmYHis')));
        //load upload class library
        $this->upload->initialize($config);
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('bukti_pembayaran'))
        {
            // case - failure
            
            $data['success_msg'] = '<div class="alert alert-danger text-center ">' . $this->upload->display_errors(). '</strong> !</div>';
            $this->load->model('login_model');
			$databarang['data']	= $this->login_model->tampil_beli($id_barang)->result_object();
			$tampil = $data + $databarang;
            $this->load->view('transaksi_view',$tampil); 

        }
        else
        {
            // case - success
            $id_transaksi['id_transaksi']			='trk-'.trim(str_replace(" ","",date('dmYHis')));
	        $nama_barang							= $this->input->post('nama_barang');
			$harga_barang 							= $this->input->post('harga_barang');
			$tanggal_transaksi 						= $this->input->post('tanggal_transaksi');
			$nama_pemesan 							= $this->input->post('nama_pemesan');
			$alamat_pemesan 						= $this->input->post('alamat_pemesan');
			$email									= $this->input->post('email');
			$id_penjual								= $this->input->post('id_penjual');
			$gambar 								= $this->input->post('gambar_barang');
            $upload_data 							= $this->upload->data();
            $file_id = array(
            			'id_transaksi'			=> $id_transaksi['id_transaksi'],
            			'nama_barang'			=> $nama_barang,
						'harga_barang'			=> $harga_barang,
						'tanggal_transaksi'		=> $tanggal_transaksi,
						'nama_pemesan'			=> $nama_pemesan,
						'alamat_pemesan'		=> $alamat_pemesan,
		         		'bukti_pembayaran' 		=> $upload_data['file_name'],
		         		'gambar'				=> $gambar,
						'email_penjual'			=> $email,
						'id_penjual'			=> $id_penjual
		         	);
            $this->load->model('login_model');
			$this->login_model->tambah_transaksi($file_id);
            $data['success_msg'] = '<div class="alert alert-success text-center ">id_transaksi <strong>' . $id_transaksi['id_transaksi'] . '</strong> telah berhasil di unggah!</div>';
			$databarang['data']	= $this->login_model->tampil_beli($id_barang)->result_object();
            $tampil = $data + $databarang;
            $this->load->view('transaksi_view', $tampil);
            //redirect('person/transaksi/'.$no,'refresh');
	            //redirect('utama','refresh');
        }
	}
    
    public function tampilorder($id)
    {
    	$this->load->model('login_model');
        $databarang['data']	= $this->login_model->tampil_order($id);
        $this->load->view('order_view', $databarang);
    }

    public function tampileditorder($id)
    {
    	$this->load->model('login_model');
        $databarang['data']	= $this->login_model->tampil_edit_order($id);
        $this->load->view('edit_order', $databarang);
    }

     public function editorder($id)
    {
    	$status_transaksi			= $this->input->post('status_transaksi');
    	$file_id = array(
						'status_transaksi'			=> $status_transaksi
		         	);
    	$this->load->model('login_model');
    	$data['data'] = $this->login_model->edit_order($id,$file_id);
		redirect('welcome','refresh');
	}

	public function tampilsearchingorder()
    {
    	//$this->load->model('login_model');
    	//$data['data'] = $this->login_model->searching_order($id);
		$this->load->view('searching_order');
	}


     public function searchingorder($id_transaksi)
    {
    	$this->load->model('login_model');
    	$data['data'] = $this->login_model->searching_order($id_transaksi);//->result_object();
		$this->load->view('searching_order', $data);
	}
}
