<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{
	
	public function __construct() {
        parent::__construct();
        $this->load->model('login_model','',TRUE);
        $this->load->library('session');
    }
	public function index()
	{
		$this->form_validation->set_rules('email, Email','trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|callback_basisdata_cek');
		if($this->form_validation->run()==false){
			$this->load->view('Login_view');
		}else{
			$this->load->model('login_model');
			$email 	= $this->input->post('email');
			$data['data'] = $this->login_model->ambil_username($email);
			$this->load->view('utama_login',$data); 
			//redirect(base_url('index.php/welcome/username/'), 'refresh');
		}
	}


	function basisdata_cek($password)
	{
		$email 	= $this->input->post('email');
		$result 	= $this->login_model->login($email,$password);
		if($result)
		{
			$sess_array = array();
			foreach ($result as $row) {
				$sess_array = $arrayname = array('id'=> $row->id, 'nama'=> $row->nama, 'email'=> $row->email);
				$this->session->set_userdata($sess_array);
			}
			return true;
		}else{
			$this->form_validation->set_message('basisdata_cek', 'Invalid email or password (email atau password salah)');
			return false;
		}
	}
}