<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model{

	var $table = 'transaksi';

	function login($email,$password){
		$this->db->select('id, nama, email, password');
		$this->db->from('user');
		$this->db->where('email', $email);
		$this->db->where('password', $password);
		$this->db->limit(1);

		$query = $this->db->get();
		if($query->num_rows()==1){
			return $query->result();
		}else{
			return false;
		}
	}

	function add($datacustomer) //reski
	{
		$this->db->insert('user', $datacustomer);
	}

	function tampil() //reski
	{
		return $this->db->get('tbl_barang');
	}

	function email_pengupload($email) //reski
	{
		$this->db->insert('tbl_barang', $email);
	}

	public function ambil_username($email)
	{
		$this->db->where("email",$email);
		return $this->db->get('user');
	}

	function tambah_barang($file_id) //reski
	{
		$this->db->insert('tbl_barang', $file_id);
		//$db = $this->mysqli_conn
		//$db->query("INSERT INTO tbl_barang VALUES ('','$nama_barang','$harga_barang','$deskripsi_barang','$gambar_barang','$alamat','$email')") or die ($db->error);
		//$this->db->insert('tbl_barang', $databarang);
	}

	public function tampil_beli($id_barang)
	{
		//$sql ='select id_barang, nama_barang, harga_barang, deskripsi_barang, gambar, alamat_penjual, email, no_rek, nama_bank from tbl_barang join user on id_penjual=id where id_barang="'.$id_barang.'"';
		//$query = $this->db->query($sql);
		//$result = $query->result();
		//return $result;
		$this->db->where("id_barang",$id_barang);
		return $this->db->get('tbl_barang');
	}

	public function tambah_transaksi($file_id)
	{
		$this->db->insert('transaksi', $file_id);
	}

	public function tampil_order($id)
	{
		$sql ='select T.id_transaksi, T.nama_barang, T.harga_barang, T.tanggal_transaksi, T.nama_pemesan, T.alamat_pemesan, T.bukti_pembayaran, T.email_penjual, T.status_transaksi, T.gambar from transaksi T join user U where T.id_penjual="'.$id.'"';
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
		//$this->db->where("email_penjual",$email);
		//return $this->db->get('transaksi');
	}

	public function username($id)
	{

		//$this->db->where("id",$id);
		//return $this->db->get('user');
		$sql ='select id,email from user U where id="'.$id.'"';
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}

	public function id_penjual($id_barang)
	{
		$sql ='select id_penjual from tbl_barang where id_barang="'.$id_barang.'"';
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}

	public function id($email)
	{

		//$this->db->where("id",$id);
		//return $this->db->get('user');
		$sql ='select id from user U where email="'.$email.'"';
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}

	public function tampil_edit_order($id)
	{
		$sql ='select T.id_transaksi, T.nama_barang, T.harga_barang, T.tanggal_transaksi, T.nama_pemesan, T.alamat_pemesan, T.bukti_pembayaran, T.email_penjual, T.status_transaksi, T.gambar from transaksi T join user U where T.id_transaksi="'.$id.'"';
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
		//$this->db->where("email_penjual",$email);
		//return $this->db->get('transaksi');
	}

	public function edit_order($id,$file_id)
	{
		//$this->db->update($this->table, $file_id, $id);
		//return $this->db->affected_rows();
		$this->db->where('id_transaksi', $id);
		$this->db->update('transaksi', $file_id);
		//$this->db->where("id_transaksi",$id_transaksi);
		//return $this->db->get('transaksi');
	}

	public function searching_order($id)
	{
		$sql ='select T.id_transaksi, T.nama_barang, T.harga_barang, T.tanggal_transaksi, T.nama_pemesan, T.alamat_pemesan, T.bukti_pembayaran, T.email_penjual, T.status_transaksi, T.gambar from transaksi T where T.id_transaksi="'.$id.'"';
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}
}