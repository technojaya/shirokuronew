<!doctype html>
<html lang="en">

<head>
    <title>SHIROKURO | ART MARKETPLACE</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Coming+Soon|Montserrat" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/font-awesome/css/font-awesome.min.css ">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/slick/slick-theme.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/slick/slick.css">
</head>

<body>
    <nav class="navbar fixed-top navbar-expand-lg bgwhite navbar-light mb-30">
        <div class="container">
            <div class="col">
                <a class="navbar-brand" href="index.html"><img class="brandimg" src="<?php echo base_url()?>assets/image/logo.png"> </a>
            </div>
            <div class="col text-right">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo site_url('welcome')?>">SHOP<span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="about.html">ABOUT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">FAQS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">CONTACT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">CONFIRM PAYMENT</a>
                        </li>
                        <!--<li class="nav-item">
                            <a class="btn btn-md" href="#">
                              <i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Login </a></li>
                                        <li><a href="#">Sign Up</a></li>
                                    </ul>
                                </li>
                        </li>-->
                </div>
                <button class="margin-tb-10 navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </div>
    </nav>
    <div class="container mt120">
        <section class="mb-30 ">
            <!-- <div class="hidden-xs-down">
                <div class="regular slider-only">
                    <div class="slick-slider-item">
                        <img src="assets/image/slide/slideshow_1.jpg" alt="" />
                    </div>
                    <div class="slick-slider-item">
                        <img src="assets/image/slide/slideshow_2.jpg" alt="" />
                    </div>
                    <div class="slick-slider-item">
                        <img src="assets/image/slide/slideshow_3.jpg" alt="" />
                    </div>
                    <div class="slick-slider-item">
                        <img src="assets/image/slide/slideshow_4.jpg" alt="" />
                    </div>
                    <div class="slick-slider-item">
                        <img src="assets/image/slide/slideshow_5.jpg" alt="" />
                    </div>
                    <div class="slick-slider-item">
                        <img src="assets/image/slide/slideshow_6.jpg" alt="" />
                    </div>
                </div>
            </div> -->
        </section>
        <hr>
        <section>
            <div class="row ">
                <div class="col-lg-12">
                    <div class="row m-t-50 login">
                        <div class="col-lg-6 hidden-xs">
                            <img alt="image" class="img-responsive center" src="<?php echo base_url()?>assets/image/frame poitrait.jpg">
                        </div>
                        <div class="col-lg-6 col-xs-12 login">
                            <div class="p-lg p-t-n">
                                <h2 class="visible-xs">Login</h2>
                                <div class="row">
                                    <div class="col-xs-5 border-bottom m-t-sm"></div>
                                    <div class="col-xs-2 text-center">
                                        <p>Atau</p>
                                    </div>
                                    <div class="col-xs-5 border-bottom m-t-sm"></div>
                                </div>

                                <?php
                                if(validation_errors()){
                                ?>
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;    </span></button>
                                    <strong><?php echo validation_errors(); ?></strong>
                                </div>
                                <?php
                                }
                                echo form_open('welcome/addregister', 'class="myclass"');
                                ?>

                                <form novalidate="novalidate">
                                    <div class="form-group">
                                            
                                            <label>Nama</label>
                                            <input type="text" class="form-control" name="nama" id="nama" tabindex="1" autofocus="" required="" aria-required="true">
                                    </div>
                                    <div class="form-group">
                                            
                                            <label>Tanggal Lahir</label>
                                            <input type="text" class="form-control" name="tanggal_lahir" id="tangga_lahir" tabindex="1" autofocus="" required="" aria-required="true">
                                    </div>
                                    <div class="form-group">
                                            
                                            <label>Alamat</label>
                                            <input type="text" class="form-control" name="alamat" id="alamat" tabindex="1" autofocus="" required="" aria-required="true">
                                    </div>
                                    <div class="form-group">
                                        
                                        <label>Email</label>
                                        <input type="email" class="form-control" name="email" id="email" tabindex="1" autofocus="" required="" aria-required="true">
                                    </div>

                                    <div class="form-group">
                                        
                                        <div class="row">
                                            <div class="col-lg-6 col-xs-4">
                                                <label>Password</label>
                                            </div>
                                            <!--<div class="col-lg-6 col-xs-8 text-right forgot-password">
                                                <a href="/forgot"><em>Lupa password?</em></a>
                                            </div>-->
                                        </div>
                                        <input type="password" class="form-control" name="password" tabindex="2" required="" aria-required="true">
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-5 col-lg-offset-2 col-xs-6 text-right">
                                        
                                            <button type="submit" class="btn btn-info" tabindex="3">Daftar</button>
                                        <?php echo form_close() ?>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
        </section>>
        <hr>
        <div class="container-fluid">
            <!-- Footer -->
            <hr>
            <footer class="container">
                <div class="row">
                    <div class="col-6 col-sm-6">
                        <h4>Newsletter</h4>
                        <p>Daftarkan email Anda untuk mendapatkan berita terbaru, produk terbaru dan penawaran khusus dari Nangningnung!</p>
                        <form class="form-horizontal p-xs">
                            <div class="row">
                                <div class="col-8">
                                    <input type="email" name="email" class="form-control" placeholder="Alamat email Anda">
                                </div>
                                <div class="col-4">
                                    <button type="submit" class="btn btn-info">Subscribe</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-4 col-sm-3">
                    </div>
                    <div class="col-4 col-sm-3">
                        <h4>Media Sosial</h4>
                        <div class="row">
                            <div class="col-lg-3 col-sm-3">
                                <a href="#"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a>
                            </div>
                            <div class="col-lg-3 col-sm-3">
                                <a href="#"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
                            </div>
                            <div class="col-lg-3 col-sm-3">
                                <a href="#"><i class="fa fa-pinterest-square fa-2x" aria-hidden="true"></i></a>
                            </div>
                            <div class="col-lg-3 col-sm-3">
                                <a href="#"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="col-xs-12">
                        <hr>
                        <p>© 2017 Shirokuro.com</p>
                    </div>
                </div>
            </footer>
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/main.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/slick/slick.min.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/slick/slick.js')?>"></script>
</body>

</html>