
<?php   
        foreach ($data as $v) {
                $email      = $v->email_penjual;
            } 
?>

<!doctype html>

<html lang="en">

<head>
    <title>SHIROKURO | ART MARKETPLACE</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/font-awesome/css/font-awesome.min.css ">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/slick/slick-theme.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/slick/slick.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light mb-30">
        <div class="container">
            <div class="col">
                <a class="navbar-brand" href="#"><img class="brandimg" src="<?php echo base_url()?>assets/image/logo.png"></a>
            </div>
            <div class="col text-right">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="<?php echo site_url('welcome')?>">SHOP<span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">ABOUT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">FAQS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">CONTACT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">CONFIRM PAYMENT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo site_url('login')?>">LOGIN</a>
                        </li>
                        <li class="nav-item">
                            <a class="btn btn-secondary btn-md" href="#">
                              <i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                        </li>
                        <li><p class="navbar-text">Selamat Datang, <?php echo $email; ?></p></li>
                </div>
                <button class="margin-tb-10 navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </div>
    </nav>
    <div class="container">
        <section class="mb-30 ">
            <div class="hidden-xs-down">
                <div class="regular slider-only">
                    <div class="slick-slider-item">
                        <img src="<?php echo base_url()?>assets/image/slide/slideshow_1.jpg" alt="" />
                    </div>
                    <div class="slick-slider-item">
                        <img src="<?php echo base_url()?>assets/image/slide/slideshow_2.jpg" alt="" />
                    </div>
                    <div class="slick-slider-item">
                        <img src="<?php echo base_url()?>assets/image/slide/slideshow_3.jpg" alt="" />
                    </div>
                    <div class="slick-slider-item">
                        <img src="<?php echo base_url()?>assets/image/slide/slideshow_4.jpg" alt="" />
                    </div>
                    <div class="slick-slider-item">
                        <img src="<?php echo base_url()?>assets/image/slide/slideshow_5.jpg" alt="" />
                    </div>
                    <div class="slick-slider-item">
                        <img src="<?php echo base_url()?>assets/image/slide/slideshow_6.jpg" alt="" />
                    </div>
                </div>
            </div>
        </section>
        <div class="text-center">
            <h4>New Item</h4>
        </div>
        <hr>
        <section>
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>ID Transaksi</th>
                        <th>Nama Barang</th>
                        <th>Harga Barang</th>
                        <th>Tanggal Transaksi</th>
                        <th >Nama Pemesan</th>
                        <th>Alamat Pemesan</th>
                        <th>Bukti Pembayaran</th> 
                        <th>Status Transaksi</th>  
                        <th>Gambar Pesanan</th>                    
                        <th style="width:195px;">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($data as $v): ?>
                        <tr>
                            <td><?php echo $v->id_transaksi ?></td>
                            <td><?php echo $v->nama_barang ?></td>
                            <td><?php echo $v->harga_barang ?></td>
                            <td><?php echo $v->tanggal_transaksi ?></td>
                            <td><?php echo $v->nama_pemesan ?></td>
                            <td><?php echo $v->alamat_pemesan ?></td>
                            <td>
                                <img src="<?php echo base_url('files/').$v->bukti_pembayaran?>" width="100px">
                            </td>
                            <td><?php echo $v->status_transaksi ?></td>
                            <td>
                                <img src="<?php echo base_url('files/').$v->gambar?>" width="100px">
                            </td>
                            <td>
                                <!--<?php echo form_open('welcome/transaksi/'.$v->id_barang);?>
                                <button type="submit" class="btn btn-info" tabindex="3">Bayar</button>
                                <?php echo form_close(); ?>
                                <!--<a class="btn btn-sm btn-danger" href="javascript:void(0)"><i class="glyphicon glyphicon-pencil" onclick="">Hapus</i></a>-->
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>

                <tfoot>
                <tr>
                        <th>ID Transaksi</th>
                        <th>Nama Barang</th>
                        <th>Harga Barang</th>
                        <th>Tanggal Transaksi</th>
                        <th >Nama Pemesan</th>
                        <th>Alamat Pemesan</th>
                        <th>Bukti Pembayaran</th> 
                        <th>Status Transaksi</th>  
                        <th>Gambar Pesanan</th>                    
                        <th style="width:195px;">Aksi</th>
                    </tr>
                </tfoot>
            </table>
            </section>
                                <?php echo form_open_multipart('welcome/editorder/'.$v->id_transaksi);?>
                                    <form action="" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Id Barang</label>
                                        <input type="text" class="form-control" name="id_transaksi" id="id_transaksi" tabindex="1" autofocus="" required="" aria-required="true" value="<?php echo $v->id_transaksi ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Nama Barang</label>
                                        <input type="text" class="form-control" name="nama_barang" id=nama_barang" tabindex="1" autofocus="" required="" aria-required="true" value="<?php echo $v->nama_barang ?>">
                                    </div>
                                    <div class="form-group">
                                            
                                            <label>Harga Barang</label>
                                            <input type="text" class="form-control" name="harga_barang" id="harga_barang" tabindex="1" autofocus="" required="" aria-required="true" value="<?php echo $v->harga_barang ?>">
                                    </div>
                                    <div class="form-group">
                                            
                                            <label>Tanggal</label>
                                            <input name="tanggal_transaksi" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text" value="<?php echo $v->tanggal_transaksi ?>">
                                            <span class="help-block"></span>
                                    </div>
                                    <div class="form-group">
                                        
                                        <label>Nama Pemesan</label>
                                        <input type="text" class="form-control" name="nama_pemesan" id="nama_pemesan" tabindex="1" autofocus="" required="" aria-required="true" value="<?php echo $v->nama_pemesan ?>">
                                    </div>
                                    <div class="form-group">
                                        
                                        <label>Alamat Pemesan</label>
                                        <input type="text" class="form-control" name="alamat_pemesan" id="alamat_pemesan" tabindex="1" autofocus="" required="" aria-required="true" value="<?php echo $v->alamat_pemesan ?>">
                                    </div>

                                    <div class="form-group">
                                        <label>Bukti Pembayaran</label>
                                        <input type="text" class="form-control" name="bukti_pembayaran" id="bukti_pembayaran" tabindex="1" autofocus="" required="" aria-required="true" value="<?php echo $v->bukti_pembayaran ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Status Transaksi</label>
                                            <select name="status_transaksi" class="form-control" value="<?php echo $v->status_transaksi ?>">
                                                <option value="">--Select Status--</option>
                                                <option value="On Progress">On Progress</option>
                                                <option value="Complete">Complete</option>
                                            </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Gambar Barang</label>
                                        <input type="text" class="form-control" name="gambar_barang" id="gambar_barang" tabindex="1" autofocus="" required="" aria-required="true" value="<?php echo $v->gambar ?>">
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-5 col-lg-offset-2 col-xs-7 text-right">
                                        
                                    
                                        </div>
                                        <div class="col-lg-7 col-lg-offset-3 col-xs-7 text-right">
                                            <button type="submit" class="btn btn-info" tabindex="3">Update</button>
                                            
                                            <button type="submit" class="btn btn-danger" tabindex="3">Batal</button>
                                        </div>
                                    </div>
                                </form>
                                <?php echo form_close(); ?>
                                <!--<?php if (isset($success_msg)) { echo $success_msg; } ?>-->
            <!--<div class="row text-center mb-30">
                <div class="card-deck pad-lr-15">
                    <div class="card">
                        <img class="card-img-top img-fluid  thumbnail" src="assets/image/AP2.jpg" alt="Card image cap">
                        <div class="card-block">
                            <h4 class="card-title">Art Paper</h4>
                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>
                        <div class="card-footer">
                            <div class="rating">
                                <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
                                <p class="price">Rp 80.000,00</p>
                            </div>
                            <div class="col-sm">
                                <a href=".." target="_blank">
                                    <button class="btn btn-info right"> Add to cart</button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <img class="card-img-top img-fluid  thumbnail" src="assets/image/AP2.jpg" alt="Card image cap">
                        <div class="card-block">
                            <h4 class="card-title">Art Paper</h4>
                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>
                        <div class="card-footer">
                            <div class="rating">
                                <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
                                <p class="price">Rp 80.000,00</p>
                            </div>
                            <div class="col-sm">
                                <a href=".." target="_blank">
                                    <button class="btn btn-info right"> Add to cart</button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="thumbnail">
                            <img class="card-img-top img-fluid thumbnail" src="assets/image/AP4.jpg" alt="Card image cap">
                        </div>
                        <div class="card-block">
                            <h4 class="card-title">Art Paper</h4>
                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>
                        <div class="card-footer">
                            <div class="rating">
                                <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
                                <p class="price">Rp 80.000,00</p>
                            </div>
                            <div class="col-sm">
                                <a href=".." target="_blank">
                                    <button class="btn btn-info right"> Add to cart</button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="thumbnail">
                            <img class="card-img-top img-fluid thumbnail" src="assets/image/AP4.jpg" alt="Card image cap">
                        </div>
                        <div class="card-block">
                            <h4 class="card-title">Art Paper</h4>
                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>
                        <div class="card-footer">
                            <div class="rating">
                                <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
                                <p class="price">Rp 80.000,00</p>
                            </div>
                            <div class="col-sm">
                                <a href=".." target="_blank">
                                    <button class="btn btn-info right"> Add to cart</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row text-center mb-30">
                <div class="card-deck pad-lr-15">
                    <div class="card">
                        <img class="card-img-top img-fluid  thumbnail" src="assets/image/AP2.jpg" alt="Card image cap">
                        <div class="card-block">
                            <h4 class="card-title">Art Paper</h4>
                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>
                        <div class="card-footer">
                            <div class="rating">
                                <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
                                <p class="price">Rp 80.000,00</p>
                            </div>
                            <div class="col-sm">
                                <a href=".." target="_blank">
                                    <button class="btn btn-info right"> Add to cart</button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <img class="card-img-top img-fluid  thumbnail" src="assets/image/AP2.jpg" alt="Card image cap">
                        <div class="card-block">
                            <h4 class="card-title">Art Paper</h4>
                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>
                        <div class="card-footer">
                            <div class="rating">
                                <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
                                <p class="price">Rp 80.000,00</p>
                            </div>
                            <div class="col-sm">
                                <a href=".." target="_blank">
                                    <button class="btn btn-info right"> Add to cart</button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="thumbnail">
                            <img class="card-img-top img-fluid thumbnail" src="assets/image/AP4.jpg" alt="Card image cap">
                        </div>
                        <div class="card-block">
                            <h4 class="card-title">Art Paper</h4>
                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>
                        <div class="card-footer">
                            <div class="rating">
                                <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
                                <p class="price">Rp 80.000,00</p>
                            </div>
                            <div class="col-sm">
                                <a href=".." target="_blank">
                                    <button class="btn btn-info right"> Add to cart</button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="thumbnail">
                            <img class="card-img-top img-fluid thumbnail" src="assets/image/AP4.jpg" alt="Card image cap">
                        </div>
                        <div class="card-block">
                            <h4 class="card-title">Art Paper</h4>
                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>
                        <div class="card-footer">
                            <div class="rating">
                                <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
                                <p class="price">Rp 80.000,00</p>
                            </div>
                            <div class="col-sm">
                                <a href=".." target="_blank">
                                    <button class="btn btn-info right"> Add to cart</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
        </section>
        <hr>
        <div class="container-fluid">
            <!-- Footer -->
            <hr>
            <footer class="container">
                <div class="row">
                    <div class="col-6 col-sm-6">
                        <h4>Newsletter</h4>
                        <p>Daftarkan email Anda untuk mendapatkan berita terbaru, produk terbaru dan penawaran khusus dari Nangningnung!</p>
                        <form class="form-horizontal p-xs">
                            <div class="row">
                                <div class="col-8">
                                    <input type="email" name="email" class="form-control" placeholder="Alamat email Anda">
                                </div>
                                <div class="col-4">
                                    <button type="submit" class="btn btn-info">Subscribe</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-4 col-sm-3">

                    </div>
                    <div class="col-4 col-sm-3">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4>Media Sosial</h4>
                                <div class="col-md-3 col-xs-6">
                                    <a href="#"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"><font size="5">@ShirokuroArt</font><br></i></a>
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <a href="#"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"><font size="5">Shirokuro.com</font><br></i></a>
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <a href="#"><i class="fa fa-pinterest-square fa-2x" aria-hidden="true"><font size="5">ShirokuroArt</font><br></i></a>
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <a href="#"><i class="fa fa-instagram fa-2x" aria-hidden="true"><font size="5">@ShirokuroArt</font><br></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="col-xs-12">
                        <hr>
                        <p>© 2017 Shirokuro.com</p>
                    </div>
                </div>
            </footer>
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
        <script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/main.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/slick/slick.min.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/slick/slick.js')?>"></script>
</body>

</html>