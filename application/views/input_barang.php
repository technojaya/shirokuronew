<?php   
        foreach ($data as $v) {
                $id         = $v->id;
                $email      = $v->email;
            } 
?>

<!doctype html>
<html lang="en">

<head>
    <title>SHIROKURO | ART MARKETPLACE</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Coming+Soon|Montserrat" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/font-awesome/css/font-awesome.min.css ">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/slick/slick-theme.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/slick/slick.css">
</head>

<body>
    <nav class="navbar fixed-top navbar-expand-lg bgwhite navbar-light mb-30">
        <div class="container">
            <div class="col">
                <a class="navbar-brand" href="index.html"><img class="brandimg" src="<?php echo base_url()?>assets/image/logo.png""> </a>
            </div>
            <div class="col text-right">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#">SHOP<span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="about.html">ABOUT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">FAQS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">CONTACT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">CONFIRM PAYMENT</a>
                        </li>
                        <!--<li class="nav-item">
                            <a class="btn btn-secondary btn-md" href="#">
                              <i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                        </li>-->
                        <li class="nav-item">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo site_url('welcome')?>">Log out</a></li>
                                    </ul>
                                </li>
                        </li>
                </div>
                <button class="margin-tb-10 navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </div>
    </nav>
    <div class="container mt120">
        <section class="mb-30 ">
            <!-- <div class="hidden-xs-down">
                <div class="regular slider-only">
                    <div class="slick-slider-item">
                        <img src="assets/image/slide/slideshow_1.jpg" alt="" />
                    </div>
                    <div class="slick-slider-item">
                        <img src="assets/image/slide/slideshow_2.jpg" alt="" />
                    </div>
                    <div class="slick-slider-item">
                        <img src="assets/image/slide/slideshow_3.jpg" alt="" />
                    </div>
                    <div class="slick-slider-item">
                        <img src="assets/image/slide/slideshow_4.jpg" alt="" />
                    </div>
                    <div class="slick-slider-item">
                        <img src="assets/image/slide/slideshow_5.jpg" alt="" />
                    </div>
                    <div class="slick-slider-item">
                        <img src="assets/image/slide/slideshow_6.jpg" alt="" />
                    </div>
                </div>
            </div> -->
        </section>
        <hr>
        <section>
            <div class="row ">
                <div class="col-lg-12">
                    <div class="row m-t-50 login">
                        <div class="col-lg-6 hidden-xs">
                            <img alt="image" class="img-responsive center" src="<?php echo base_url()?>assets/image/frame poitrait.jpg">
                        </div>
                        <div class="col-lg-6 col-xs-12 login">
                            <div class="p-lg p-t-n">
                                <h2 class="visible-xs">Login</h2>
                                <div class="row">
                                    <div class="col-xs-5 border-bottom m-t-sm"></div>
                                    <div class="col-xs-2 text-center">
                                        <p>Atau</p>
                                    </div>
                                    <div class="col-xs-5 border-bottom m-t-sm"></div>
                                </div>
                                <?php echo form_open_multipart('welcome/tambahbarang');?>
                                <form action="" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                            
                                            <label>Nama Barang</label>
                                            <input type="text" class="form-control" name="nama_barang" id="nama_barang" tabindex="1" autofocus="" required="" aria-required="true">
                                    </div>
                                    <div class="form-group">
                                            
                                            <label>Harga</label>
                                            <input type="number" class="form-control" name="harga_barang" id="harga_barang" tabindex="1" autofocus="" required="" aria-required="true">
                                    </div>
                                    <div class="form-group">
                                            
                                            <label>Deskripsi Barang</label>
                                            <input type="text" class="form-control" name="deskripsi_barang" id="deskripsi_barang" tabindex="1" autofocus="" required="" aria-required="true">
                                    </div>

                                    <div class="form-group">
                                            
                                            <label>Gambar Barang</label>
                                            <input type="file" class="form-control" name="gambar_barang" id="gambar_barang" tabindex="1" autofocus="" required="" aria-required="true">
                                    </div>

                            <!--<fieldset>
                                <div class="form-group col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="filename" class="control-label">Tambah Gambar</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-12">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <input type="file" name="filename" size="20" />
                                            <div style="position:relative;">
                                                    <a class='btn btn-default btn-file' href='javascript:;'>
                                                        Choose File...
                                                        <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="filename" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
                                                    </a>
                                                    &nbsp;
                                                    <span class='label label-default' id="upload-file-info"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <input type="submit" value="Unggah Berkas" class="btn btn-primary"/>
                                            <span class="label label-danger"><?php if (isset($error)) { echo $error; } ?></span>
                                        </div>
                                        <div class="col-md-7">
                                            <i><p>Ukuran File Maksimum 5 MB</p>
                                            <p>Format File: JPG | JPEG </p></i>
                                        </div>
                                    </div>
                                </div>
                                </fieldset>-->
                                    <div class="form-group">
                                        
                                        <label>Alamat</label>
                                        <input type="text" class="form-control" name="alamat" id="alamat" tabindex="1" autofocus="" required="" aria-required="true">
                                    </div>
                                    <div class="form-group">
                                        
                                        <label>email</label>
                                        <input type="email" class="form-control" name="email" id="email" tabindex="1" autofocus="" required="" aria-required="true" value="<?php echo $email; ?>">
                                    </div>
                                    <div class="form-group">
                                        
                                        <label>ID Penjual</label>
                                        <input type="text" class="form-control" name="id" id="id" tabindex="1" autofocus="" required="" aria-required="true" value="<?php echo $id; ?>">
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-5 col-lg-offset-2 col-xs-6 text-right">
                                        
                                            <button type="submit" class="btn btn-info" name="tambah" tabindex="3">TAMBAH</button>
                                        </div>
                                    </div>
                                </form>
                                <!--<?php
                                    if(@$_POST['tambah'])
                                    {
                                        $nama_barang            = $connection->conn->real_escape_string($_POST['nama_barang']);
                                        $harga_barang           = $connection->conn->real_escape_string($_POST['harga_barang']);
                                        $deskripsi_barang       = $connection->conn->real_escape_string($_POST['deskripsi_barang']);
                                        $alamat                 = $connection->conn->real_escape_string($_POST['alamat']);
                                        $email                  = $connection->conn->real_escape_string($_POST['email']);

                                        $extensi = explode(".".$_FILES['gambar_barang']['name']);
                                        $gambar_barang = "brg-".round(microtime(true).".".end($extensi));
                                        $sumber = $_FILES['gambar_barang']['tmp_name'];
                                        $upload = move_uploaded_file($sumber, "files".$gambar_barang);
                                        if($upload)
                                        {
                                        $brg->tambah_barang($nama_barang, $harga_barang, $deskripsi_barang,$gambar_barang,$alamat,$email);
                                        redirect('welcome/halaman_utama_login');
                                        }
                                    }
                                ?>-->
                                <?php echo form_close(); ?>
                                <?php if (isset($success_msg)) { echo $success_msg; } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
        </section>>
        <hr>
        <div class="container-fluid">
            <!-- Footer -->
            <hr>
            <footer class="container">
                <div class="row">
                    <div class="col-6 col-sm-6">
                        <h4>Newsletter</h4>
                        <p>Daftarkan email Anda untuk mendapatkan berita terbaru, produk terbaru dan penawaran khusus dari Nangningnung!</p>
                        <form class="form-horizontal p-xs">
                            <div class="row">
                                <div class="col-8">
                                    <input type="email" name="email" class="form-control" placeholder="Alamat email Anda">
                                </div>
                                <div class="col-4">
                                    <button type="submit" class="btn btn-info">Subscribe</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-4 col-sm-3">
                    </div>
                    <div class="col-4 col-sm-3">
                        <h4>Media Sosial</h4>
                        <div class="row">
                            <div class="col-lg-3 col-sm-3">
                                <a href="#"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a>
                            </div>
                            <div class="col-lg-3 col-sm-3">
                                <a href="#"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
                            </div>
                            <div class="col-lg-3 col-sm-3">
                                <a href="#"><i class="fa fa-pinterest-square fa-2x" aria-hidden="true"></i></a>
                            </div>
                            <div class="col-lg-3 col-sm-3">
                                <a href="#"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="col-xs-12">
                        <hr>
                        <p>© 2017 Shirokuro.com</p>
                    </div>
                </div>
            </footer>
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/slick/slick.min.js"></script>
        <script type="text/javascript" src="assets/slick/slick.js"></script>
</body>

</html>